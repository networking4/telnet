#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <iostream>
using std::cout;
using std::cin;

#include "platf_dep.hpp"
#include "common.hpp"

int main(int argc, char *argv[])
{
#ifdef _WIN32
	win_init();
#endif

	int sockfd, numbytes;
	char buf[MY_MAX_SIZE];
	struct addrinfo hints, *servinfo, *p;
	int rv;
	char s[INET6_ADDRSTRLEN], hostname[MY_MAX_SIZE];
    char cr = 'n'; // criterion of sorting
    bool isCrValid = false;

	if (argc < 2) {
	    cout << "Input hostname: ";
	    cin.getline(hostname, MY_MAX_SIZE);
	} else {
        strcpy(hostname, argv[1]);
        // if criterion of sorting was omitted then sort by name as default
        if (argc == 2) {
            isCrValid = true;
        } else if (argc == 3 && argv[2][0] == '-') {
            const char c = argv[2][1];
            // checking if third parameter is equal one of available type of sorting
            if ('n' == c || 'e' == c || 't' == c) {
                cr = c;
                isCrValid = true;
            }
        } else if (argc == 4 && 0 == strcmp(argv[2], "--by") ) {
            // checking if fourth parameter is equal one of criterions from array
            for (int i = 0; i < 3; ++i) {
                if (0 == strcmp(argv[3], cr_arr[i])) {
                    cr = argv[3][0];
                    isCrValid = true;
                    break;
                }
            }
        }
        if (argc < 3) {

        }
	}

    while (!isCrValid) {
        cout << "Choose the criterion of sorting you want client to perform:\n";
        cout << "n --- sorting by name\n";
        cout << "e --- sorting by extension\n";
        cout << "t --- sorting by time\n";
        cout << "Enter one letter (n|e|t): ";
        cin.getline(buf, MY_MAX_SIZE);
        if ('\0' == buf[1] ) {
            cr = buf[0];
        }
        if ('n' == cr || 'e' == cr || 't' == cr) {
            isCrValid = true;
        }
    }

    cout << "Requesting on sorting by " << get_full(cr) << "...\n";

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if ((rv = getaddrinfo(hostname, MY_TELNET_PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// loop through all the results and connect to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("client: socket");
			continue;
		}

		if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			perror("client: connect");
			my_close(sockfd);
			continue;
		}

		break;
	}

	if (p == NULL) {
		fprintf(stderr, "client: failed to connect\n");
		return 2;
	}

   inet_ntop(p->ai_family, get_in_addr((struct sockaddr*)p->ai_addr),
        s, sizeof s);

	cout << "Connecting to " << s << "...\n";

	freeaddrinfo(servinfo); // all done with this structure

    buf[0] = cr;
    buf[1] = '\0';
    if (send(sockfd, buf, 1, 0) == -1)
			perror("send");

	if ((numbytes = recv(sockfd, buf, MY_MAX_SIZE-1, 0)) == -1) {
	    perror("recv");
	    exit(1);
	}

	buf[numbytes] = '\0';

	cout << "Response from remote HOST: " << buf << " files were sorted in folder and subfolders.\n";

#ifdef _WIN32
    closesocket(sockfd);
    WSACleanup();
#else
	close(sockfd);
#endif

	return 0;
}
