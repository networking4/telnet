TARGETS=sortreq client
SOURCES=common.cpp platf_dep.cpp
HEADERS=common.hpp platf_dep.hpp
DEPENDENCIES=$(SOURCES) $(HEADERS)

.PHONY: all clean pristine

all: $(TARGETS)

pristine: clean

#CLIENT FLAG defines name of exetuble file
CL_FL=
#sortreq FLAG defines name of exetuble file
SORT_FL=

#I solved this by looking for an env
#variable that will only be set on windows.
#Because %OS% is the type of windows, it should be
#set on all Windows computers but not on Linux.
ifdef OS

CC=cl
CCOPTS=/std:c++17

objects=common.obj sortreq.obj client.obj fyle_sys.obj
clean:
	del $(objects)

else
    ifeq ($(shell uname), Linux)

CC=g++
CCOPTS=-std=c++17 -Wall -Wextra
CL_FL=-o client
SORT_FL=-o sortreq

clean:
	rm -f $(TARGETS)

    endif
endif

client: $(DEPENDENCIES) file_sys.cpp file_sys.hpp
	$(CC) $(CCOPTS) $(CL_FL) client.cpp $(SOURCES) file_sys.cpp
sortreq: $(DEPENDENCIES)
	$(CC) $(CCOPTS) $(SORT_FL) sortreq.cpp $(SOURCES)
