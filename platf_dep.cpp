// platf_dep.cpp --- platform dependent stuff

#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include "platf_dep.hpp"

//returns last modification time for
//file with path from argument
time_t get_time(const char *path) {
#ifdef _WIN32
    struct _stat buf;
#else
    struct stat buf;
#endif

    int result; // result of call

      // Get data associated with "crt_stat.c":
#ifdef _WIN32
    result = _stat(path, &buf);
#else
    result = stat(path, &buf);
#endif

    // Check if statistics are valid:
    if (result != 0)
    {
        std::cout << "get_time function: error\n";
    }
    else
    {
        return buf.st_mtime;
    }
    return 0;
}

#ifdef _WIN32

void win_init() {
    WSADATA wsaData;
    WORD wVersionRequested;
    int err;

    wVersionRequested = MAKEWORD(2, 2);

    err = WSAStartup(wVersionRequested, &wsaData);
    if (err != 0) {
        /* Tell the user that we could not find a usable */
        /* WinSock DLL.                                  */
        std::cout << "WSAStartup failed.\n";
        /*return -1;*/
    }

    /* Confirm that the WinSock DLL supports 2.2.*/
    /* Note that if the DLL supports versions greater    */
    /* than 2.2 in addition to 2.2, it will still return */
    /* 2.2 in wVersion since that is the version we      */
    /* requested.                                        */
    if (LOBYTE(wsaData.wVersion) != 2 ||
        HIBYTE(wsaData.wVersion) != 2) {
        /* Tell the user that we could not find a usable */
        /* WinSock DLL.                                  */
        WSACleanup();
        std::cout << "We could not find a usable WinSock DLL.\n";
        /*return -1;*/
    }
    /* The WinSock DLL is acceptable. Proceed. */
}

//Declaring this function explicitly in my code.
const char* inet_ntop(int af, const void* src, char* dst, socklen_t size)
{
    struct sockaddr_storage ss;
    unsigned long s = size;

    ZeroMemory(&ss, sizeof(ss));
    ss.ss_family = af;

    switch (af) {
    case AF_INET:
        ((struct sockaddr_in*)&ss)->sin_addr = *(struct in_addr*)src;
        break;
    case AF_INET6:
        ((struct sockaddr_in6*)&ss)->sin6_addr = *(struct in6_addr*)src;
        break;
    default:
        return NULL;
    }
    /* cannot direclty use &size because of strict aliasing rules */
    return (WSAAddressToString((struct sockaddr*)&ss, sizeof(ss), NULL, dst, &s) == 0) ?
        dst : NULL;
}
#endif

