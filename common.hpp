/*
    common.hpp includes declarations that
    are common for sortreq.cpp and client.cpp
*/

//prefix MY is added in order to omit conflict with other macros
//(this problem didn't allow to compile for windows)
#define MY_TELNET_PORT "3490" // the port client will be connecting to
#define MY_MAX_SIZE 100 // max number of bytes we can get at once

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa);

//MY_N - max number of letters for word that describes criterion of sorting
#define MY_N 10

extern char cr_arr[4][MY_N];

// returns full name of criterion from shorthand (char)
char* get_full(char cr);

// returns flag for bash ls command depending on shorthand (char)
char* ls_flag(char cr);
