#pragma once

/*
*	returns multiline text with content of folder and
	subfolders sorted by name, or extension, or time
	depending on second argument
	if type == 'n' --- sorting by name
	if type == 'e' --- sorting by extension
	if type == 't' --- sorting by time

	In variable addressed by pointer files_number 
	number of sorted files will be written. 
*/
std::string folder_content(fs::path path, char type, 
					unsigned *files_number = nullptr);